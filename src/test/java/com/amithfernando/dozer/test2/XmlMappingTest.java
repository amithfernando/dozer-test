/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.amithfernando.dozer.test2;

import com.amithfernando.dozer.test2.model.SourceModel;
import com.amithfernando.dozer.test2.model.TargetModel;
import java.util.Date;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;

/**
 *
 * @author USER
 */
public class XmlMappingTest {

    private XmlMapping xmlMapping;

    @Before
    public void initialize() {
        xmlMapping = new XmlMapping();
    }

    @Test
    public void testConvertSource() {
        SourceModel sourceModel = new SourceModel();
        sourceModel.setName("Amith Fernando");
        sourceModel.setAge(23);
        sourceModel.setDateOfBirth("20/06/2016 16:00:33");
        //
        TargetModel targetModel = xmlMapping.convertSource(sourceModel);
        //
        assertNotNull(targetModel);
        assertEquals("Amith Fernando", targetModel.getTargetName());
        assertEquals(23, targetModel.getTargetAge());
        assertEquals(16, targetModel.getTargetDateOfBirth().getHours());
        assertEquals(33, targetModel.getTargetDateOfBirth().getSeconds());
    }

}
