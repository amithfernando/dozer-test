/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.amithfernando.dozer.test1;

import com.amithfernando.dozer.test1.model.SourceModel;
import com.amithfernando.dozer.test1.model.TargetModel;
import java.util.Date;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;

/**
 *
 * @author Amith
 */
public class BasicMappingTest {

    private BasicMapping firstMapping;

    @Before
    public void initialize() {
        firstMapping = new BasicMapping();
    }

    @Test
    public void testConvertSource() {
        SourceModel sourceModel = new SourceModel();
        sourceModel.setName("Amith Fernando");
        sourceModel.setAge(23);
        sourceModel.setDateOfBirth(new Date());
        //
        TargetModel targetModel = firstMapping.convertSource(sourceModel);
        assertNotNull(targetModel);
        assertEquals("Amith Fernando", targetModel.getName());
        assertEquals(23, targetModel.getAge());
        //
    }

    @Test
    public void testConvertTarget() {
        TargetModel targetModel = new TargetModel();
        targetModel.setName("Amith Fernando");
        targetModel.setAge(23);
        targetModel.setDateOfBirth(new Date());
        //
        SourceModel sourceModel= firstMapping.convertTarget(targetModel);
        assertNotNull(sourceModel);
        assertEquals("Amith Fernando", sourceModel.getName());
        assertEquals(23, sourceModel.getAge());

    }

}
