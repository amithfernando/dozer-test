/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.amithfernando.dozer.test2.model;

import java.util.Date;

/**
 *
 * @author Amith
 */
public class TargetModel {

    private String targetName;
    private int targetAge;
    private Date targetDateOfBirth;

    public String getTargetName() {
        return targetName;
    }

    public void setTargetName(String targetName) {
        this.targetName = targetName;
    }

    public int getTargetAge() {
        return targetAge;
    }

    public void setTargetAge(int targetAge) {
        this.targetAge = targetAge;
    }

    public Date getTargetDateOfBirth() {
        return targetDateOfBirth;
    }

    public void setTargetDateOfBirth(Date targetDateOfBirth) {
        this.targetDateOfBirth = targetDateOfBirth;
    }

  
    
    
    
    
}
