/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.amithfernando.dozer.test2;

import com.amithfernando.dozer.test2.model.SourceModel;
import com.amithfernando.dozer.test2.model.TargetModel;
import java.util.ArrayList;
import java.util.List;
import org.dozer.DozerBeanMapper;

/**
 *
 * @author Amith
 */
public class XmlMapping {

    public TargetModel convertSource(SourceModel sourceModel) {
        List myMappingFiles = new ArrayList();
        myMappingFiles.add("xml-mapping.xml");
        DozerBeanMapper mapper = new DozerBeanMapper();
        mapper.setMappingFiles(myMappingFiles);
        TargetModel targetModel = mapper.map(sourceModel, TargetModel.class,"xml-mapping");
        return targetModel;
    }

}
