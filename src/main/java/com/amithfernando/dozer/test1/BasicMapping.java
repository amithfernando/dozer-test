/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.amithfernando.dozer.test1;

import com.amithfernando.dozer.test1.model.SourceModel;
import com.amithfernando.dozer.test1.model.TargetModel;
import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;

/**
 *
 * @author Amith
 */
public class BasicMapping {

    public TargetModel convertSource(SourceModel sourceModel) {
        Mapper mapper = new DozerBeanMapper();
        TargetModel targetModel = mapper.map(sourceModel, TargetModel.class);
        return targetModel;

    }

    public SourceModel convertTarget(TargetModel targetModel) {
        Mapper mapper = new DozerBeanMapper();
        SourceModel sourceModel = mapper.map(targetModel, SourceModel.class);
        return sourceModel;

    }

}
